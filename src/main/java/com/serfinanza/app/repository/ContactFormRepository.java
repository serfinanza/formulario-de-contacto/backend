package com.serfinanza.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.serfinanza.app.entity.ContactFormEntity;

public interface ContactFormRepository extends JpaRepository<ContactFormEntity, Long>{

}
