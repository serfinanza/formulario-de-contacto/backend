package com.serfinanza.app.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.serfinanza.app.entity.ContactFormEntity;
import com.serfinanza.app.repository.ContactFormRepository;

@RestController
@RequestMapping("/contact/form")
@CrossOrigin(origins = "http://localhost:8080/")
public class ContactFormRest {

	@Autowired
	private ContactFormRepository contactFormRepository;
		
	@PostMapping
	public ResponseEntity<ContactFormEntity> store(@RequestBody ContactFormEntity contactFormEntity) {
		
		ContactFormEntity mContactFormEntity = contactFormRepository.save(contactFormEntity);

		return ResponseEntity.ok(mContactFormEntity);
		
	}
}
