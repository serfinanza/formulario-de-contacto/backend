package com.serfinanza.app.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("contact/form/country")
@CrossOrigin(origins = "http://localhost:8080/")
public class CountryRest {

	@GetMapping
	public String index() {

		String uri = "http://country.io/names.json";
		
		RestTemplate restTemplate = new RestTemplate();
		
		String resut = restTemplate.getForObject(uri, String.class);

		return resut;

	}

}
